package com.fuseelab.popularmovies;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marco on 23-May-17.
 */

public class GridAdapter extends BaseAdapter {
    private Context mContext;
    private List<GridItem> mGridData = new ArrayList<>();
    private final String IMG_ROOT_URL = "http://image.tmdb.org/t/p/w300";

    private final String LOG_TAG = GridAdapter.class.getSimpleName();

    public GridAdapter(Context mContext, ArrayList<GridItem> mGridData) {
        super();
        this.mContext = mContext;
        this.mGridData = mGridData;
    }

    public void setGridData(List<GridItem> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mGridData.size();
    }

    @Override
    public Object getItem(int position) {
        return mGridData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final GridItem item = mGridData.get(position);
        ImageView imageView;

        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setPadding(0, 5, 0, 0);
            convertView = imageView;
        } else {
            imageView = (ImageView) convertView;
        }

        Log.d(LOG_TAG, "Loading image from " + IMG_ROOT_URL + item.getPosterPath());
        Picasso.with(mContext).load(IMG_ROOT_URL + item.getPosterPath()).into(imageView);

        return convertView;
    }
}
