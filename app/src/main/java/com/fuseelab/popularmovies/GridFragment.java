package com.fuseelab.popularmovies;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GridFragment extends Fragment {

    private final String LOG_TAG = GridFragment.class.getSimpleName();
    private final String DATA_BY_POPULAR = "popular";
    private final String DATA_BY_RANK = "top_rated";

    private GridAdapter mGridAdapter;
    private List<GridItem> movieDataset= new ArrayList<>();

    public GridFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        FetchMoviewTask fetchMovie = new FetchMoviewTask();
        fetchMovie.execute(DATA_BY_POPULAR);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_grid, container, false);

        // GridView definition
        GridView gridView = (GridView) rootView.findViewById(R.id.gridview_movies);
        mGridAdapter = new GridAdapter(gridView.getContext(), (ArrayList<GridItem>) movieDataset);
        gridView.setAdapter(mGridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GridItem selectedMovie = (GridItem) mGridAdapter.getItem(position);
                Intent intent = new Intent(getActivity(), DetailActivity.class).putExtra("SELECTED_MOVIE", selectedMovie);
                startActivity(intent);
            }
        });

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        FetchMoviewTask fetchMovie = new FetchMoviewTask();
        if (id == R.id.sort_popular_action) {
            fetchMovie.execute(DATA_BY_POPULAR);
            return true;
        }
        if (id == R.id.sort_rank_action) {
            fetchMovie.execute(DATA_BY_RANK);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class FetchMoviewTask extends AsyncTask<String, Void, GridItem[]> {

        private final String LOG_TAG = FetchMoviewTask.class.getSimpleName();

        @Override
        protected GridItem[] doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            String moviesJsonString;
            String selector = params[0] + "?"; // single element

            try {
                final String BASE_URL = "https://api.themoviedb.org/3/movie/";
                final String KEY_PARAM = "api_key";

                Uri builtUri = Uri.parse(BASE_URL + selector)
                        .buildUpon()
                        .appendQueryParameter(KEY_PARAM, BuildConfig.THE_MOVIE_DB_API_KEY)
                        .build();

                URL url = new URL(builtUri.toString());

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                moviesJsonString = buffer.toString();

                return moviesDataFromJson(moviesJsonString);
            } catch (IOException | JSONException e) {
                Log.e(LOG_TAG, "Error ", e);
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }
        }

        private GridItem[] moviesDataFromJson(String moviesJsonString) throws JSONException {

            // These are the names of the JSON objects that need to be extracted.
            final String OWM_RESULTS = "results";
            final String OWM_POSTER = "poster_path";
            final String OWM_OVERVIEW = "overview";
            final String OWM_RELEASE_DATE = "release_date";
            final String OWM_TITLE = "original_title";
            final String OWM_VOTE = "vote_average";

            JSONObject moviesJson = new JSONObject(moviesJsonString);
            JSONArray moviesList = moviesJson.getJSONArray(OWM_RESULTS);

            GridItem[] result = new GridItem[moviesList.length()];

            for (int i = 0; i < moviesList.length(); i++) {
                JSONObject starterJson = moviesList.getJSONObject(i);

                GridItem movie = new GridItem();
                movie.setOriginalTitle(starterJson.getString(OWM_TITLE));
                movie.setPosterPath(starterJson.getString(OWM_POSTER));
                movie.setOverview(starterJson.getString(OWM_OVERVIEW));
                movie.setVoteAverage(starterJson.getDouble(OWM_VOTE));
                movie.setReleaseDate(starterJson.getString(OWM_RELEASE_DATE));

                result[i] = movie;
            }

            return result;
        }

        @Override
        protected void onPostExecute(GridItem[] result) {
            if (result != null) {
                movieDataset.clear();
                for (int i = 0; i < result.length; i++) {
                    movieDataset.add(result[i]);
                    Log.d(LOG_TAG, "Item inserted: " + result[i].toString());
                }
                mGridAdapter.setGridData(movieDataset);
            } else {
                Toast.makeText(getContext(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
            }

            Log.d(LOG_TAG, "Total items inserted: " + movieDataset.size());
        }
    }
}
