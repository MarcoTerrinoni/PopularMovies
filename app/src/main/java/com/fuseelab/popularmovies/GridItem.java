package com.fuseelab.popularmovies;

import java.io.Serializable;

/**
 * Created by marco on 22-May-17.
 */

public class GridItem implements Serializable {
    private String originalTitle;
    private String posterPath;
    private String overview;
    private double voteAverage;
    private String releaseDate;

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public String toString() {
        return "[originalTitle: " + originalTitle +
                ", posterPath: " + posterPath +
                ", overview:" + overview +
                ", voteAverage: " + String.valueOf(voteAverage) +
                ", releaseDate: " + releaseDate +
                "]";
    }
}
