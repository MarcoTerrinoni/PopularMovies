package com.fuseelab.popularmovies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class DetailFragment extends Fragment {
    private final String IMG_ROOT_URL = "http://image.tmdb.org/t/p/w500";

    private GridItem selectedMovie;

    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        // The detail Activity called via intent.  Inspect the intent for forecast data.
        Intent intent = getActivity().getIntent();
        if (intent != null && intent.hasExtra("SELECTED_MOVIE")) {
            selectedMovie = (GridItem) intent.getSerializableExtra("SELECTED_MOVIE");

            ((TextView) rootView.findViewById(R.id.movie_title))
                    .setText(selectedMovie.getOriginalTitle());

            ImageView imageView = (ImageView) rootView.findViewById(R.id.movie_poster);
            Picasso.with(getContext()).load(IMG_ROOT_URL + selectedMovie.getPosterPath()).into(imageView);

            ((TextView) rootView.findViewById(R.id.movie_overview))
                    .setText(selectedMovie.getOverview());

            ((TextView) rootView.findViewById(R.id.movie_vote_average))
                    .setText(String.valueOf(selectedMovie.getVoteAverage()));

            ((TextView) rootView.findViewById(R.id.movie_release_date))
                    .setText(selectedMovie.getReleaseDate());


        }

        return rootView;
    }
}
